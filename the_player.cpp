//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i){
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

void ThePlayer::handleNextRowOfVideos (int left, int right) {
    // to index the appropriate button at a given row
    int at = 0;

    // iterates second row of videos
    if(left == 0) {
        at=4;
    }
    // to index the video to be hidden when next is clicked
    int lastVideoInRow = at + 3;

    // the index of the video to be referenced for the button at
    int to=left;

    // iterates for all buttons in the row
    for (to,at; to < right; to++, at++) {
        setButtonToVideo(at,to);
    }

    // hides the last video of the row in order to not repeat
    hideVideo(lastVideoInRow);
};

// handles the rendering of the videos that were shown before next was clicked
void ThePlayer::handlePreviousRowOfVideos (bool isFirstRow) {

    // maths below to restore the previous state of the appropriate row
    if (isFirstRow) {
        int at=0, to=0;
        for (at,to; at < 3; at++, to++) {
            setButtonToVideo(at,to);
        }
        showVideo(3);
    }

    else {
        int at=4, to=3;
        for (at,to; at < 7; at++, to++) {
            setButtonToVideo(at,to);
        }
        showVideo(7);
    }
}

// handles the change of buttons to differing videos
void ThePlayer::setButtonToVideo(int at, int to) {
    // get the video info to which the button will be changed to
    TheButtonInfo *i = &infos->at(to);
    // change the button at given index to the extracted button info
    buttons->at(at)->init(i);
}

// hides the video at specified index
void ThePlayer::hideVideo(int at) {
    buttons->at(at)->setVisible(false);
}

// displays the video at specified index
void ThePlayer::showVideo(int at) {
    buttons->at(at)->setVisible(true);
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}
