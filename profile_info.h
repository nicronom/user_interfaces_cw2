#ifndef CW2_PROFILE_INFO_H
#define CW2_PROFILE_INFO_H

#include <QWidget>
#include "register_form.h"

using namespace std;

// This component is responsible to render the profile
// page of a user, from a registration user form supplied.
class ProfileInfo : public QWidget {
  Q_OBJECT

// fields to reference the objects contained within
private:
  QWidget *timeWidget_;
  Form userInfo_;
  QLabel *email_;
  QLabel *age_;
  QSpinBox *time_;
  QLabel *currentRestriction_;
  QLabel *timeChangedMessage_;
  QPushButton *changeTime_;
  QPushButton *newTimeSubmit_;

  QString setStyling(bool isLabel);

private slots:
  // handles the rendering of labels and fields required
  // to update the time limit of a user
  void handleChangeTimeClick();
  // handles the submission logic of time limit change
  void handleSubmitNewTimeClick();

public:
  ProfileInfo(Form userInfo);
  ~ProfileInfo();

};

#endif //CW2_PROFILE_INFO_H
