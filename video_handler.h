#ifndef CW2_EXTRACT_VIDEOS_H
#define CW2_EXTRACT_VIDEOS_H

#include "the_button.h"
#include <vector>

using namespace std;

class VideoHandler {
private:
  // list of video lists
  // ( useful if subfolders are introduced ) OR
  // videos need to be divided into their separate sections
  vector<vector<TheButtonInfo>> *videoLists;
public:
  // the video list is initially empty
  VideoHandler() {
      this->videoLists = NULL;
  }
  // extracts the videos from given exact path
  vector<vector<TheButtonInfo>> *getInfoIn(string path);
  vector<TheButtonInfo> getVideos(string path);
};

#endif
