#ifndef CW2_REGISTER_FORM_H
#define CW2_REGISTER_FORM_H

#include <QtWidgets>
#include <QLabel>
#include <QLineEdit>

struct Form {
  QString emailInput;
  QString passwordInput;
  int ageInput;
  int timeInput;
  bool epilepsyChecked_;
  bool colorBlindnessChecked_;
  bool bothChecked_;
};

// The registration form component. When rendered,
// the user will be able to fill in their details such
// as email , password, child's age and time restriction for
// videos per day.
class RegisterForm: public QWidget {

Q_OBJECT

private:
  // private fields to reference the labels and inputs of the form
  QLabel *emailLabel_;
  QLineEdit *emailInput_;
  QLabel *passwordLabel_;
  QLineEdit *passwordInput_;
  QLabel *ageLabel_;
  QSpinBox *ageInput_;
  QLabel *timeLabel_;
  QSpinBox *timeInput_;
  QRadioButton *epilepsy_;
  QRadioButton *colorBlindness_;
  QRadioButton *both_;
  QString setStyling();

public:
  RegisterForm(QWidget * = 0);

public slots:
  // to access and submit the current values of the fields
  Form getInputs();

};


#endif //CW2_REGISTER_FORM_H
