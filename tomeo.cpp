/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for no reason
 *
 * 2811 cw2 November 2019 by twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QtWidgets/QHBoxLayout>
#include "the_player.h"
#include "tomeo_window.h"

using namespace std;

// the application starts from here
int main(int argc, char *argv[]) {
    // create the Qt Application
    QApplication app(argc, argv);

    // create the main window and layout
    TomeoWindow window(argc, argv);

    // showtime!
    window.show();

    // wait for the app to terminate
    return app.exec();
}
