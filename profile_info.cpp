#include "profile_info.h"

ProfileInfo::ProfileInfo(Form userInfo) {
    this->userInfo_ = userInfo;

    // the layout for the profile widget
    QVBoxLayout *layout = new QVBoxLayout();

    // labels to be displayed about the user's email and child age from input
    string emailText = "Email: " + this->userInfo_.emailInput.toStdString();
    email_ = new QLabel(QString::fromStdString(emailText));
    email_->setStyleSheet(setStyling(true));

    string ageText = "Child's age: " + to_string(this->userInfo_.ageInput);
    age_ = new QLabel(QString::fromStdString(ageText));
    age_->setStyleSheet(setStyling(true));

    // displays the current time restriction per day set on videos for this account
    string timeToString = to_string(userInfo_.timeInput);
    string labelText = "Current time restriction: " + timeToString + " minutes";
    currentRestriction_ = new QLabel(QString::fromStdString(labelText));
    currentRestriction_->setStyleSheet(setStyling(true));

    // the button which will render the spin box to update current time limit
    changeTime_ = new QPushButton("Change restriction");
    changeTime_->connect(changeTime_, SIGNAL(released()), this, SLOT(handleChangeTimeClick()));
    changeTime_->setStyleSheet(setStyling(false));

    // the spinbox to control value of and set a new time restriction for child
    time_ = new QSpinBox();
    time_->setMinimum(0);
    // maximum number of minutes
    time_->setMaximum(24*60);
    time_->setVisible(false);

    // the submit button for the updated time restriction
    newTimeSubmit_ = new QPushButton("Submit");
    newTimeSubmit_->setVisible(false);
    newTimeSubmit_->connect(newTimeSubmit_, SIGNAL(released()), this, SLOT(handleSubmitNewTimeClick()));
    newTimeSubmit_->setStyleSheet(setStyling(false));

    // the message to be displayed when the time limit has been successfully updated
    timeChangedMessage_ = new QLabel("Time limit is now updated");
    timeChangedMessage_->setVisible(false);
    timeChangedMessage_->setStyleSheet("color: red; font-size: 26px; ");

    // the layout, which renders the time restriction section of the profile page
    QHBoxLayout *timeLayout = new QHBoxLayout();
    timeLayout->addWidget(currentRestriction_);
    timeLayout->addWidget(changeTime_);
    timeLayout->addWidget(time_);
    timeLayout->addWidget(newTimeSubmit_);
    timeLayout->addWidget(timeChangedMessage_);

    // the container widget for the non-static parts on updates
    timeWidget_ = new QWidget();
    timeWidget_->setLayout(timeLayout);

    // finally bundle everything together
    layout->addWidget(email_);
    layout->addWidget(age_);
    layout->addWidget(timeWidget_);
    setLayout(layout);
}

// handles the rendering of the section to allow the user
// to update the current time limit set
void ProfileInfo::handleChangeTimeClick() {
    // the info was not updated yet, hide the message
    timeChangedMessage_->setVisible(false);
    // display the spinbox
    time_->setVisible(true);
    // display the submit button
    newTimeSubmit_->setVisible(true);
    // hide the current restriction
    currentRestriction_->setVisible(false);
    // hide the change time button as it was clicked on
    // hence the user wants to update the time
    changeTime_->setVisible(false);
}

// handles the rendering of the profile page to notify
// the user that the time limit has been changed
void ProfileInfo::handleSubmitNewTimeClick() {
    userInfo_.timeInput = time_->value();
    time_->setVisible(false);

    string labelText = "Current time restriction: " + to_string(userInfo_.timeInput) + " minutes";
    currentRestriction_->setText(QString::fromStdString(labelText));
    currentRestriction_->setVisible(true);
    timeChangedMessage_->setVisible(true);

    newTimeSubmit_->setVisible(false);
    changeTime_->setVisible(true);
}

QString ProfileInfo::setStyling(bool isLabel) {
    bool isColorBlind = userInfo_.colorBlindnessChecked_ || userInfo_.bothChecked_;

    if (isLabel) {
        if (isColorBlind) {
            return "color: #00b8e6; font-size: 26px";
        }
        else {
            return "color: purple; font-size: 26px";
        }
    }

    if (isColorBlind) {
        return "background-color: #00b8e6; color: white; border: 4px solid blue; font-size: 32px;";
    }


    return "background-color: yellow ; color: purple; border: 1px solid purple; font-size: 32px;";
}

// dereferences and releases the appropriate memory
ProfileInfo::~ProfileInfo() {
    delete this->email_;
    delete this->age_;
    delete this->time_;
    delete this->changeTime_;
    delete this->newTimeSubmit_;
    delete this->currentRestriction_;
    delete this->timeChangedMessage_;
    delete this->timeWidget_;
    delete this->layout();
    delete this;
}