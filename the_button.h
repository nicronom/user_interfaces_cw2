#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H

#include <QtWidgets/QPushButton>

// Component responsible for handling the logic of
// a button with a video image landscape. Forwards
// the user to the appropriate video on click.
class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display

    TheButtonInfo ( QUrl* url, QIcon* icon) : url (url), icon (icon) {}
};

class TheButton : public QPushButton {
    Q_OBJECT

public:
    TheButtonInfo* info;

     TheButton(QWidget *parent) :  QPushButton(parent) {
         setIconSize(QSize(400,220));
         connect(this, SIGNAL(released()), this, SLOT (clicked() ));
    }

    void init(TheButtonInfo* i);

private slots:
    // function handler for clicking on a video button
    void clicked();

signals:
    // starts playing the video referenced by the button
    void jumpTo(TheButtonInfo*);

};

#endif //CW2_THE_BUTTON_H
