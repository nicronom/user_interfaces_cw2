#ifndef CW2_TOMEO_WINDOW_H
#define CW2_TOMEO_WINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QVideoWidget>
#include <QLineEdit>
#include <QLabel>
#include "video_handler.h"
#include "the_player.h"
#include "register_form.h"
#include "profile_info.h"
#include <iostream>

using namespace std;

// The class which holds the window of the application.
// This component is responsible to handle the logic of
// and rendering of all sublayouts and subsystems.
class TomeoWindow: public QWidget{

Q_OBJECT

private:
  int argc_;
  string path_;

  // a list of the buttons
  vector<TheButton*> buttons_;
  // a list of the videos
  vector<TheButtonInfo> videos_;

  // references to widgets to be destroyed after
  // the user has logged in the system
  QPushButton *loginButton_;
  QPushButton *signInButton_;
  QPushButton *logo_;

  //boolean to indicate if the user registered
  bool registered_ = false;

  // widgets which will be hidden on certain events
  vector<QWidget *> widgetsToHide_;
  // the video, which is being played
  QVideoWidget *theVideo_;
  // the subtitles tracker
  QPushButton *subtitles_;
  // the button to hide the video being played
  QPushButton *hideButton_;
  // structure to hold the information about user on register
  Form userInfo_;
  ProfileInfo *profileInfo_;

  // the labels for the genre sections
  QLabel *moviesLabel_;
  QLabel *seriesLabel_;

public:
  TomeoWindow(int argc, char *argv[]);

protected:

private:
  QVBoxLayout *makeLoginLayout();
  QVBoxLayout *makeRegisterLayout();
  QHBoxLayout *makeNavBarLayout();
  QHBoxLayout *makeFeaturedLayout(ThePlayer *player);
  QVBoxLayout * makeLayout();
  QWidget *makeButtonRow(int leftIndex, int rightIndex, ThePlayer *player);
  QHBoxLayout *makeRowOfVideos(int leftIndex, int rightIndex, ThePlayer *player, QWidget *buttonWidget, QHBoxLayout *layout);
  QString setStyling(bool isLabel);

  void deleteWidgets();
  void displayWidgets(bool arg);

private slots:
  void setClickedState ();
  void setRegisterClickedState();
  void setSubmitClickedState();

};


#endif //CW2_TOMEO_WINDOW_H
