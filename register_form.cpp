#include "register_form.h"

RegisterForm::RegisterForm(QWidget *) {
    // Label indicators and the input for email
    emailInput_ = new QLineEdit();
    emailLabel_ = new QLabel("Email:");
    emailLabel_->setBuddy(emailInput_);
    emailLabel_->setStyleSheet(setStyling());

    // Label indicators and the input for password
    passwordInput_ = new QLineEdit();
    passwordLabel_ = new QLabel("Password:");
    passwordLabel_->setBuddy(passwordInput_);
    passwordLabel_->setStyleSheet(setStyling());

    // Label indicators and the input for age
    ageInput_ = new QSpinBox();
    // can't have negative age
    ageInput_->setMinimum(0);
    // realistic age is expected
    ageInput_->setMaximum(150);
    ageLabel_ = new QLabel("Child Age:");
    ageLabel_->setBuddy(ageInput_);
    ageLabel_->setStyleSheet(setStyling());

    // Label indicators and the input for time restriction
    timeInput_ = new QSpinBox();
    // can't have negative minutes
    timeInput_->setMinimum(0);
    // maximum number of minutes per day
    timeInput_->setMaximum(24*60);
    timeLabel_ = new QLabel("Time restriction (minutes): ");
    timeLabel_->setStyleSheet(setStyling());

    timeLabel_->setBuddy(timeLabel_);

    QVBoxLayout *formLayout = new QVBoxLayout();

    formLayout->addWidget(emailLabel_);
    formLayout->addWidget(emailInput_);
    formLayout->addWidget(passwordLabel_);
    formLayout->addWidget(passwordInput_);
    formLayout->addWidget(ageLabel_);
    formLayout->addWidget(ageInput_);
    formLayout->addWidget(timeLabel_);
    formLayout->addWidget(timeInput_);

    QLabel *initQuestion = new QLabel("Does your child have epilepsy or colorblindness?");
    initQuestion->setStyleSheet(setStyling());
    QLabel *explanation = new QLabel("Please choose one:");
    explanation->setStyleSheet(setStyling());

    epilepsy_ = new QRadioButton("Epilepsy");
    epilepsy_->setStyleSheet(setStyling());
    colorBlindness_ = new QRadioButton("Colorblindness");
    colorBlindness_->setStyleSheet(setStyling());
    both_ = new QRadioButton("Both");
    both_->setStyleSheet(setStyling());
    QRadioButton *none = new QRadioButton("N/A");
    none->setStyleSheet(setStyling());

    formLayout->addWidget(initQuestion);
    formLayout->addWidget(explanation);
    formLayout->addWidget(epilepsy_);
    formLayout->addWidget(colorBlindness_);
    formLayout->addWidget(both_);
    formLayout->addWidget(none);

    setLayout(formLayout);
}

QString RegisterForm::setStyling() {
    return "color: purple; font-size: 26px;";
}

// returns the current state of the input fields
Form RegisterForm::getInputs() {
    return {
        emailInput_->text(),
        passwordInput_->text(),
        ageInput_->value(),
        timeInput_->value(),
        epilepsy_->isChecked(),
        colorBlindness_->isChecked(),
        both_->isChecked()
    };
}