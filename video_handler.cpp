#include "video_handler.h"
#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QImageReader>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"

using namespace std;

vector<vector<TheButtonInfo>> * VideoHandler::getInfoIn(string path) {
    this->videoLists = new vector<vector<TheButtonInfo>>();

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(path) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();
        // it's not an image, then it must be a video
        if (!f.contains(".png")) {
            // extracts the name of the file, without extension
            QString thumb = f.left( f .length() - 4) +".png";
            // thumbnail of the same name as the video exists
            if (QFile(thumb).exists()) {
                QImageReader *imageReader = new QImageReader(thumb);
                // attempts to read the thumbnail
                QImage sprite = imageReader->read();
                // successfully read
                if (!sprite.isNull()) {
                    QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // creates the icon of the button
                    QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                    out.push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                }
                else {
                    cerr << "warning: skipping video because I couldn't process thumbnail " << thumb.toStdString()
                         << endl;
                }
            }
            else {
                cerr << "warning: skipping video because I couldn't find thumbnail " << thumb.toStdString() << endl;
            }
        }
    }

    videoLists->push_back(out);
    vector<vector<TheButtonInfo>> *myVideos = this->videoLists;
    return myVideos;
}

vector<TheButtonInfo> VideoHandler::getVideos(string path) {
    // to hold the videos of the containing folder
    vector<TheButtonInfo> videos;

    // program argument not supplied
    vector<vector<TheButtonInfo>> *myList = getInfoIn(path);
    videos = myList[0][0];
    if(videos.size() == 0){
        cerr << "Please, check if you have the appropriate path setup for videos" << endl;
    }

    return videos;
}

