#include "tomeo_window.h"

TomeoWindow::TomeoWindow(int argc, char *argv[]) {
    this->argc_ = argc;
    // program argument supplied
    if (argc_ > 1) {
        path_ = argv[1];
    } else {
        path_ = "/tmp/sc18s2h_videos/";
    }

    setWindowTitle("tomeo");
    // the app is intended for desktop widescreen use
    setMinimumSize(1920, 900);
    // initiliases fields, to have reference if user does not register
    this->registered_ = false;
    this->userInfo_.bothChecked_ = false;
    this->userInfo_.epilepsyChecked_ = false;
    setLayout(makeLoginLayout());
}

// Responsible to render the login page of the tomeo app
QVBoxLayout * TomeoWindow::makeLoginLayout() {
    QVBoxLayout *top = new QVBoxLayout();
    // handles the maths to position the buttons appropriately
    top->setGeometry(this->rect());
    QRect r = this->rect();
    top->setContentsMargins(r.width() / 3, r.height() / 3, r.width() / 3, r.height() / 3);

    // Loads and sets the tomeo app logo, displayed above the login and register buttons
    QPixmap image(":/logo.png");
    QPushButton *logo = new QPushButton();
    logo->setIcon(image);
    logo->setIconSize(QSize(r.width() ,r.height() / 4.65));
    logo->setStyleSheet("background-color: yellow; border: 1px solid purple;");
    this->logo_ = logo;

    QPushButton *loginButton = new QPushButton("Login");
    // store the address, so that parent can delete them when user logs in (render purposes)
    this->loginButton_ = loginButton;

    QPushButton *signInButton = new QPushButton("Register");
    this->signInButton_ = signInButton;

    // styles and positions the two buttons
    loginButton->setStyleSheet(setStyling(false));
    loginButton->setMinimumHeight(r.height() / 8);
    signInButton->setStyleSheet(setStyling(false));
    signInButton->setMinimumHeight(r.height() / 8);

    // when clicked, render the primary layout of tomeo app
    connect(loginButton, SIGNAL(released()), this, SLOT(setClickedState()));
    connect(signInButton, SIGNAL(released()), this, SLOT(setRegisterClickedState()));
    top->addWidget(logo);
    top->addWidget(loginButton);
    top->addWidget(signInButton);
    return top;
}

QVBoxLayout * TomeoWindow::makeRegisterLayout() {

    // the form to be displayed for the user to fill
    RegisterForm *form = new RegisterForm();
    // when submitted, pass the inputs to the class field to store
    // and render the appropriate layout
    QPushButton *submit = new QPushButton("Submit");
    submit->setStyleSheet(setStyling(false));
    connect(submit, &QPushButton::clicked, [=] {
        this->userInfo_ = form->getInputs();
        if (userInfo_.colorBlindnessChecked_ || userInfo_.bothChecked_) {
            cerr << "Rendering colorblind layout" << endl;
        }
        delete form;
        delete submit;
        this->setSubmitClickedState();
    });

    // indicate that the user has registered
    registered_ = true;

    QVBoxLayout *registerLayout = new QVBoxLayout();
    registerLayout->addWidget(form);
    registerLayout->addWidget(submit);

    return registerLayout;
}

// Responsible to render the logged in primary layout of tomeo app
QVBoxLayout * TomeoWindow::makeLayout() {

    // instantiates the class member which handles videos
    VideoHandler videosHandler = VideoHandler();
    // collects all the videos in the folder
    this->videos_ = videosHandler.getVideos(this->path_);

    // the widget that will show the video
    theVideo_ = new QVideoWidget; // to be able to reference on event triggers

    // initially no video is selected, do not display player
    theVideo_->setVisible(false);
    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(theVideo_);

    hideButton_ = new QPushButton("Hide Video");
    hideButton_->setStyleSheet(setStyling(false));
    connect(hideButton_, &QPushButton::clicked, [=] {
      theVideo_->setVisible(false);
      displayWidgets(true);
      hideButton_->setVisible(theVideo_->isVisible());
      profileInfo_->setVisible(false);
    });
    subtitles_ = new QPushButton("<<The subtitles being displayed>>");
    subtitles_->setStyleSheet("background-color: black; color: white; font-size: 26px;");
    subtitles_->setVisible(theVideo_->isVisible());
    hideButton_->setVisible(theVideo_->isVisible());

    // to split the 7 buttons evenly
    int half = round(this->videos_.size() / 2) + 1;

    // create the first row of buttons
    QWidget *buttonWidget = makeButtonRow(0,half,player);

    // create next layer of buttons
    QWidget *buttonWidget2 = makeButtonRow(half-1, this->videos_.size(), player);

    // tell the player what buttons and videos are available
    player->setContent(&this->buttons_, &this->videos_);
    player->setVolume(0);

    // widget to hold the user info supplied on register
    profileInfo_ = new ProfileInfo(this->userInfo_);
    // not visible until profile tab is clicked
    profileInfo_->setVisible(false);

    moviesLabel_ = new QLabel("Movies");
    moviesLabel_->setStyleSheet("color: purple; font-size: 26px");
    moviesLabel_->setVisible(false);

    seriesLabel_ = new QLabel("Series");
    seriesLabel_->setStyleSheet("color: purple; font-size: 26px");
    seriesLabel_->setVisible(false);

    if (registered_ && (userInfo_.bothChecked_ || userInfo_.colorBlindnessChecked_)) {
        moviesLabel_->setStyleSheet("color: blue; font-size: 26px");
        seriesLabel_->setStyleSheet("color: blue; font-size: 26px");
    }

    QVBoxLayout *top = new QVBoxLayout();
    // add the navbar layout
    top->addLayout(makeNavBarLayout());
    // add the featured and currently watching section
    top->addLayout(makeFeaturedLayout(player));
    // add the video and hide video to the top most level
    top->addWidget(theVideo_);
    top->addWidget(subtitles_);
    top->addWidget(hideButton_);
    // add the movies section next (hidden when genre is not clicked)
    top->addWidget(moviesLabel_);
    // the buttons below movies
    top->addWidget(buttonWidget);
    // the series section (hidden when genre is not clicked)
    top->addWidget(seriesLabel_);
    top->addWidget(buttonWidget2);
    // the profile info (hidden until profile tab is clicked)
    top->addWidget(profileInfo_);
    // after a video is selected, the button selectors need to be hidden
    this->widgetsToHide_.push_back(buttonWidget);
    this->widgetsToHide_.push_back(buttonWidget2);

    return top;
}

// renders the navigation bar for the tomeo app
QHBoxLayout *TomeoWindow::makeNavBarLayout() {
    QHBoxLayout *navLayout = new QHBoxLayout();
    QHBoxLayout *leftLayout = new QHBoxLayout();
    QHBoxLayout *rightLayout = new QHBoxLayout();

    // returns to the landing page, where a video can be selected
    QPushButton *homeButton = new QPushButton("Home");
    homeButton->setStyleSheet(setStyling(false));
    connect(homeButton, &QPushButton::clicked, [=] {
      displayWidgets(true);
      moviesLabel_->setVisible(false);
      seriesLabel_->setVisible(false);
      profileInfo_->setVisible(false);
    });

    // shows the genre sections
    QPushButton *genresButton = new QPushButton("Genres");
    genresButton->setStyleSheet(setStyling(false));
    connect(genresButton, &QPushButton::clicked, [=] {
      moviesLabel_->setVisible(true);
      seriesLabel_->setVisible(true);
      displayWidgets(true);

      theVideo_->setVisible(false);
      hideButton_->setVisible(false);
      subtitles_->setVisible(false);
      profileInfo_->setVisible(false);
    });

    // shows the user profile information, provided on register
    QPushButton *profileButton = new QPushButton("Profile");
    profileButton->setStyleSheet(setStyling(false));
    connect(profileButton, &QPushButton::clicked, [=] {
      // if the user registered before logging in, display the appropriate information
      if (registered_) {
          theVideo_->setVisible(false);
          moviesLabel_->setVisible(false);
          seriesLabel_->setVisible(false);
          displayWidgets(false);
          profileInfo_->setVisible(true);
      } // send an error message indicating that the user had to register first
      else {
          profileInfo_->setVisible(false);
          cerr << "You did not register" << endl;
      }
    });

    leftLayout->addWidget(homeButton);

    // the genres section is only visible to children above 6 years old
    if (!registered_ || userInfo_.ageInput > 6) {
        leftLayout->addWidget(genresButton);
    }
    else if(registered_ && userInfo_.ageInput <= 6) {
        cerr << "Child's age is beyond 6, rendering simple layout";
        genresButton->setVisible(false);
    }

    rightLayout->addWidget(profileButton);

    navLayout->addLayout(leftLayout);
    navLayout->addLayout(rightLayout);
    return navLayout;
}

// renders the layout for featured and currently watching video
QHBoxLayout *TomeoWindow::makeFeaturedLayout(ThePlayer *player) {
    QHBoxLayout *layout = new QHBoxLayout();
    QHBoxLayout *featuredLayout = new QHBoxLayout();
    QHBoxLayout *watchingLayout = new QHBoxLayout();

    QPushButton *featuredButton = new QPushButton("FEATURED");
    featuredButton->setStyleSheet(setStyling(false));

    QPushButton *watchingButton = new QPushButton("CURRENTLY WATCHING");
    watchingButton->setStyleSheet(setStyling(false));

    featuredLayout->addWidget(featuredButton);
    watchingLayout->addWidget(watchingButton);

    // sets the media to play the pre-set video,
    // which is the featured one for tomeo app
    connect(featuredButton, &QPushButton::clicked, [=]{
      int size = widgetsToHide_.size();
      // buttons to select video no longer needed
      displayWidgets(false);

      // the first video is the selected featured video
      player->jumpTo(buttons_[0]->info);
      theVideo_->setVisible(true);
      hideButton_->setVisible(theVideo_->isVisible());
      subtitles_->setVisible(theVideo_->isVisible());
      player->setVolume(80);
      profileInfo_->setVisible(false);
      moviesLabel_->setVisible(false);
      seriesLabel_->setVisible(false);
    });

    connect(watchingButton, &QPushButton::clicked, [=]{
      displayWidgets(false);

      // the currently watching video will be displayed
      theVideo_->setVisible(true);
      hideButton_->setVisible(theVideo_->isVisible());
      subtitles_->setVisible(theVideo_->isVisible());
      profileInfo_->setVisible(false);
      moviesLabel_->setVisible(false);
      seriesLabel_->setVisible(false);
      player->setVolume(80);
    });

    layout->addLayout(featuredLayout);
    layout->addLayout(watchingLayout);

    return layout;
}

// makes a row of buttons, which act as selectors for the video to be played
QWidget *TomeoWindow::makeButtonRow(int leftIndex, int rightIndex, ThePlayer *player) {
    QWidget *buttonWidget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    buttonWidget->setLayout(layout);
    makeRowOfVideos(leftIndex, rightIndex, player, buttonWidget, layout);

    return buttonWidget;
}

// Creates a row of videos, in a specified range, player and widgets
QHBoxLayout *TomeoWindow::makeRowOfVideos(int leftIndex, int rightIndex,
                                          ThePlayer *player, QWidget *buttonWidget,
                                          QHBoxLayout *layout) {

    // to indicate which row of video buttons we are going to iterate on
    bool isFirstRow = true;

    // right index reaches the maximum video size, this is row 2
    if (rightIndex == videos_.size()) {
        isFirstRow = false;
    }

    // the buttons which will handle swiping to the next or prev videos
    QPushButton *prevButton = new QPushButton("Prev");
    prevButton->setStyleSheet(setStyling(false));
    QPushButton *nextButton = new QPushButton("Next");
    nextButton->setStyleSheet(setStyling(false));
    prevButton->setVisible(false);
    nextButton->setVisible(true);

    // previous button goes on the left
    layout->addWidget(prevButton);

    // when clicked, renders the videos on the right
    connect(nextButton, &QPushButton::clicked, [=] {

      prevButton->setVisible(true);
      // no more videos available, hides itself
      nextButton->setVisible(false);

      // the maths which determine how to address the appropriate button
      // to a video which was not visible before next is clicked
      int left = leftIndex, right = rightIndex;
      if (!isFirstRow) {
          left = 0;
          right = leftIndex;
      }
      else {
          left = rightIndex;
          right = videos_.size();
      }
      player->handleNextRowOfVideos(left, right);
    });

    // when clicked, renders the videos on the left
    connect(prevButton, &QPushButton::clicked, [=] {

      nextButton->setVisible(true);
      // hide itself as no previous videos are available
      prevButton->setVisible(false);

      // the method which will handle the rendering
      player->handlePreviousRowOfVideos(isFirstRow);
    });

    for ( int i = leftIndex; i < rightIndex; i++ ) {
        // add the margin on the left
        layout->addSpacing(24);

        TheButton *button = new TheButton(buttonWidget);
        button->setStyleSheet(setStyling(false));
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )),
                        player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play the video
        // this notifies the layout to render the screen for media to be played
        connect(button, &QPushButton::clicked, [=]{
          int size = widgetsToHide_.size();
          // buttons to select video no longer needed
          displayWidgets(false);
          moviesLabel_->setVisible(false);
          seriesLabel_->setVisible(false);

          // the appropriate video should now be displayed
          theVideo_->setVisible(true);
          hideButton_->setVisible(theVideo_->isVisible());
          subtitles_->setVisible(theVideo_->isVisible());
          player->setVolume(80);
        });
        this->buttons_.push_back(button);
        layout->addWidget(button);
        button->init(&this->videos_.at(i));

    }

    layout->addWidget(nextButton);
}

QString TomeoWindow::setStyling(bool isLabel) {
    bool isColorBlind =
        registered_ && (userInfo_.colorBlindnessChecked_ || userInfo_.bothChecked_);

    if (isLabel) {
        if (isColorBlind) {
            return "background-color: #00b8e6; color: white; font-size: 26px";
        }
        else {
            return "background-color: yellow ; color: purple; font-size: 26px";
        }
    }

    if (isColorBlind) {
        return "background-color: #00b8e6; color: white; border: 4px solid blue; font-size: 32px;";
    }


    return "background-color: yellow ;" "color: purple; border: 4px solid purple; font-size: 32px;";
}

// SLOT: handler for login button click
void TomeoWindow::setClickedState() {
    this->deleteWidgets();
    // remove the current layout
    delete this->layout();
    // sets the primary layout
    this->setLayout(makeLayout());
}


// SLOT: handler for rendering the register page
void TomeoWindow::setRegisterClickedState() {
    this->deleteWidgets();
    delete this->layout();
    this->setLayout(makeRegisterLayout());
}

// SLOT: handler for submitting the registration information
void TomeoWindow::setSubmitClickedState() {
    delete this->layout();
    this->setLayout(makeLayout());
}

// releases the memory of unused login page buttons,
// as they are no longer needed when user has logged in
void TomeoWindow::deleteWidgets() {
    delete(this->loginButton_);
    delete(this->signInButton_);
    delete(this->logo_);
}

// utility function to hide and show selected widgets cross layouts
void TomeoWindow::displayWidgets(bool arg) {
    for (int i=0; i < this->widgetsToHide_.size(); i++) {
        this->widgetsToHide_[i]->setVisible(arg);
    }
    hideButton_->setVisible(theVideo_->isVisible());
    subtitles_->setVisible(theVideo_->isVisible());
}